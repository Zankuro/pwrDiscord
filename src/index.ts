import term from "terminal-kit";
import dotenv from "dotenv";
import log_symbols from "log-symbols";

import Discord from "./discord_api/discord";
import Cli from "./cli/cli";
import { isExternalModuleReference } from "typescript";

dotenv.config();
const iterm = term.terminal;

if (process.env.token === "" || process.env.token === undefined) {
  console.log(
    log_symbols.error,
    'Error! No token provided. Make sure you have the file ".env" in the root directory with the key "token=yourtoken" '
  );
} else if (process.env.token === "yourtoken") {
  console.log(log_symbols.error, "I mean your real discord token");
}

const discord = new Discord();

////////////////////

(async () => {
  if (await discord.test_connection()) {
    const cli = new Cli();
    const i = await discord.get("messages", "585905156080664586", 5);
    //cli.print_conversation(i);
    //discord.listen("setup");
  }
})();
