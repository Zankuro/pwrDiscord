import {
  ClientRequest,
  ClientRequestArgs,
  IncomingMessage,
  OutgoingHttpHeaders,
} from "http";
import http from "https";

import WebSocket from "ws";

import {
  User,
  Recipient,
  Channels,
  Messages,
  Author,
  Attachment,
} from "./types";

import * as _ from "lodash";
import { ifError } from "assert";

class Discord {
  cache: any;

  constructor() {
    this.cache = { s: 1 }; //placeholder
  }

  set_cache(new_cache: object) {
    this.cache = new_cache;
  }

  set_heartbeat_interval(interval: number) {
    setInterval(() => {
      const ws = new WebSocket("wss://gateway.discord.gg");
      ws.on("open", () => {
        ws.send({
          op: 1,
          d: this.cache.s,
        });
      });
      console.log("Sent heartbeat...");
    }, interval);
  }

  test_connection(): Promise<boolean> {
    const promise = new Promise<boolean>((resolve, reject) => {
      const options: ClientRequestArgs = {
        method: "GET",
        hostname: "discord.com",
        port: null,
        path: "/api/v8/users/@me/channels",
        headers: {
          authorization: process.env.token?.toString(),
          "Content-Length": "0",
        },
      };
      const req: ClientRequest = http.request(
        options,
        (res: IncomingMessage) => {
          const chunks: any = [];

          res.on("data", (chunk: any) => {
            chunks.push(chunk);
          });

          req.on("error", (error) => {
            console.error(error);
          });

          res.on("end", () => {
            const body = Buffer.concat(chunks);
            if (typeof body !== undefined) {
              /*
              const i = JSON.parse(body.toString());
              console.log(i[0].id);
              */
              resolve(true);
            }
          });
        }
      );

      req.end();
    });

    return promise;
  }

  req(req: "get", path: string): Promise<string> {
    const options: ClientRequestArgs = {
      method: "GET",
      hostname: "discord.com",
      port: null,
      path: path,
      headers: {
        authorization: process.env.token?.toString(),
        "Content-Length": "0",
      },
    };

    const promise = new Promise<string>((resolve, reject) => {
      const req: ClientRequest = http.request(
        options,
        (res: IncomingMessage) => {
          const chunks: any = [];

          res.on("data", (chunk: any) => {
            chunks.push(chunk);
          });

          req.on("error", (error) => {
            console.error(error);
          });

          res.on("end", () => {
            const body = Buffer.concat(chunks);
            if (typeof body !== undefined) {
              resolve(body.toString());
            }
          });
        }
      );
      req.end();
    });
    return promise;
  }

  get(type: "messages", channel_id: string, limit: number): Promise<[Messages]>;
  get(type: "channels"): Promise<[Channels]>;
  get(type: "dms"): Promise<[User]>;

  get(type: any, channel_id?: string): Promise<any> {
    const promise = new Promise<any>(async (resolve, reject) => {
      if (type == "dms") {
        const dms: string = await this.req("get", "/api/v8/users/@me/channels");
        const dms_json: object = JSON.parse(dms);
        resolve(<[User]>dms_json);
      } else if (type == "channels") {
        const channels: string = await this.req(
          "get",
          "/api/v8/users/@me/guilds"
        );
        const channels_json: object = JSON.parse(channels);
        resolve(<[Channels]>channels_json);
      } else if (type == "messages") {
        const channels: string = await this.req(
          "get",
          `/api/v8/channels/${channel_id}/messages?limit=${5}`
        );
        const channels_json: object = JSON.parse(channels);
        resolve(<[Messages]>channels_json);
      } else {
        console.error("Invalid input");
        return;
      }
    });

    return promise;
  }

  listen(type: "setup"): Promise<boolean>;
  listen(type: "msg", callback: (msg: [Messages]) => void): void;

  async listen(type: any, callback?: (msg: [Messages]) => void): Promise<any> {
    if (type == "setup") {
      // For future generations!
      const _url = await this.req("get", "https://discord.com/api/v8/gateway");
      const url = JSON.parse(_url).url;
      const ws = new WebSocket(url);

      ws.on("open", () => {
        const identify: Object = {
          op: 2,
          d: {
            token: process.env.token?.toString(),
            intents: 1 << 12,
            properties: {
              $os: "templeOS",
              $browser: "pwrDiscord",
              $device: "pwrDicord",
            },
          },
        };
        ws.send(JSON.stringify(identify));
      });

      ws.on("message", (data) => {
        this.set_cache(JSON.parse(data.toString()));
        if (JSON.parse(data.toString()).d.heartbeat_interval !== undefined) {
          this.set_heartbeat_interval(
            JSON.parse(data.toString()).d.heartbeat_interval
          );
        } else {
          console.log("Msg recived!");
        }
      });
    }
  }
}
export default Discord;
