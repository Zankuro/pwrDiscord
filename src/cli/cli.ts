import {
  User,
  Recipient,
  Channels,
  Messages,
  Author,
  Attachment,
} from "../discord_api/types";

import * as _ from "lodash";

class Cli {
  constructor() {}

  // This is one of the most complex functions... And is a pain in the ass...
  print_conversation(ctx: [Messages]) {
    if (ctx == undefined) {
      console.error("Invalid parameters.");
      return;
    }

    _.forEachRight(ctx, (value, index) => {
      console.log(
        `[${index}]`,
        `${value.author.username} > `,
        `${value.content}`
      );
    });
  }
}

export default Cli;
